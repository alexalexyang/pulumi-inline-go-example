package fluxcd

import (
	"fmt"
	"github.com/oun/pulumi-flux/sdk/go/flux"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

func CreateNewProvider(ctx *pulumi.Context, kubeconfigPath string, private_key string) (*flux.Provider, error) {

	provider, err := flux.NewProvider(ctx, "flux", &flux.ProviderArgs{
		Git: &flux.ProviderGitArgs{
			Branch: pulumi.String("main"),
			Url:    pulumi.String("ssh://git@gitlab.com/alexalexyang/fluxcd-multitenancy-test"),
			Ssh: &flux.ProviderGitSshArgs{
				PrivateKey: pulumi.String(private_key),
				Username:   pulumi.String("git"),
			},
		},
		Kubernetes: &flux.ProviderKubernetesArgs{
			ConfigPath: pulumi.String(kubeconfigPath),
		},
	})
	if err != nil {
		return nil, err
	}
	return provider, nil
}

func BootStrapFlux(ctx *pulumi.Context, provider *flux.Provider, repoDirPath string) error {

	_, err := flux.NewFluxBootstrapGit(ctx, "this", &flux.FluxBootstrapGitArgs{
		Path: pulumi.String(repoDirPath),
	}, pulumi.Provider(provider))

	if err != nil {
		return fmt.Errorf("error bootstrapping Flux: %v", err)
	}
	return nil

}

package certManager

import (
	"fmt"

	"github.com/pulumi/pulumi-command/sdk/go/command/remote"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"

	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/core/v1"
	helmv3 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/helm/v3"

	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/meta/v1"
)

// https://github.com/pulumi/pulumi-kubernetes/issues/1917
func InstallCertManager(ctx *pulumi.Context, dependsOn *remote.Command) error {
	componentName := "cert-manager"

	namespace, err := corev1.NewNamespace(ctx, componentName, &corev1.NamespaceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Name: pulumi.String(componentName),
		},
	},
		pulumi.DependsOn([]pulumi.Resource{dependsOn}))
	if err != nil {
		return fmt.Errorf("error creating namespace: %s", err)
	}

	_, err = helmv3.NewRelease(ctx, componentName, &helmv3.ReleaseArgs{
		Chart:     pulumi.String(componentName),
		Name:      pulumi.String(componentName),
		Namespace: namespace.Metadata.Name(),
		RepositoryOpts: helmv3.RepositoryOptsArgs{
			Repo: pulumi.String("https://charts.jetstack.io/"),
		},
		Version: pulumi.String("v1.12.0"),
		// Values: pulumi.Map{
		// 	// serviceAccount: true,
		// 	// InstallCRDs:     true,
		// },
	},
		pulumi.DependsOn([]pulumi.Resource{dependsOn, namespace}),
	)

	if err != nil {
		return fmt.Errorf("error creating helm release: %s", err)
	}
	return nil
}

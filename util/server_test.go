package util

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReplaceString(t *testing.T) {
	t.Parallel()

	res, err := ReplaceSubString(
		"server.*6443",
		"more strings here - server.*6443 - more strings again",
		fmt.Sprintf("server: https://%s:6443", "123.456.789"))
	assert.NoError(t, err)

	fmt.Println(res)

	assert.Equal(t, "more strings here - server: https://123.456.789:6443 - more strings again", res)
}

# Pulumi inline Go example

This repo is mainly an exploratory tool and teaching aid.

It:
- [x] Brings up servers in Hetzner
- [x] Installs RKE2 servers and agents in them
- [x] Bootstraps Flux
- [x] Has some unit tests

There are difficulties installing Rancher 2 and cert-manager. I'll probably delete their packages later. I'm now focusing on installing them through Flux instead.

Actually, installing Rancher 2 was sort of ok. But it depends on cert-manager, which failed.

For Flux, I'm using its [multi-tenancy example](https://github.com/fluxcd/flux2-multi-tenancy). My own repo for that is private for now because there are secrets in it. Temporary but still, 😅.


## Helpful commands

`clear && $HOME/go/bin/go1.20.3 clean -testcache && $HOME/go/bin/go1.20.3 test -v ./fluxcd`


## References

[Unit testing in Pulumi](https://www.pulumi.com/docs/using-pulumi/testing/unit/)
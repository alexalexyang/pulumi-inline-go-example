package fluxcd

import (
	"context"
	"os"
	"pulumi-inline-go/config"
	getPulumi "pulumi-inline-go/pulumi-installer"
	pulumiUtil "pulumi-inline-go/pulumi-util"

	"testing"

	"github.com/joho/godotenv"
	"github.com/pulumi/pulumi/sdk/v3/go/auto"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"

	"github.com/stretchr/testify/assert"
)

// Requires k8s cluster
func TestFluxCdIntegration(t *testing.T) {

	destroy := true

	config.SetSecrets()
	config.SetConfig()
	config.SetHetznerConfig()
	getPulumi.GetPulumiWorkspaceBackendDir()
	getPulumi.GetPulumi()

	err := godotenv.Load(".env")
	if err != nil {
		t.Fatal("Error loading .env file: ", err)
	}

	private_key := os.Getenv("GITLAB_SSH_PRIVATE_KEY")
	if private_key == "" {
		t.Fatal("GITLAB_SSH_PRIVATE_KEY env var not set")
	}

	ctx := context.Background()
	stackName := "test-stack"
	projectName := config.Config.ProjectName
	var opts = pulumiUtil.GetPulumiStackArgs(stackName)

	kubeconfigPath := "/Users/alex.yang/.kube/config"

	deployFunc := func(ctx *pulumi.Context) error {
		provider, err := CreateNewProvider(ctx, kubeconfigPath, private_key)
		if err != nil {
			t.Log("Error creating flux provider: ", err)
			return err
		}

		t.Log("provider: ", provider)

		err = BootStrapFlux(ctx, provider, "clusters/staging")
		if err != nil {
			t.Logf("Error bootstrapping flux: %v", err)
			return err
		}

		return nil
	}

	stack, err := auto.UpsertStackInlineSource(ctx, stackName, projectName, deployFunc, opts...)
	if err != nil {
		t.Logf("Error with UpsertStackInlineSource: %v", err)
	}

	if destroy {
		stack.Cancel(ctx)
		res, err := stack.Destroy(ctx)
		if err != nil {
			t.Logf("destroy failed, err: %v", err)
		}

		t.Log("destroy result: ", res)

		err = os.Unsetenv("PULUMI_CONFIG_PASSPHRASE")
		if err != nil {
			t.Logf("failed to unset EnvVar.")
		}

		err = stack.Workspace().RemoveStack(ctx, stack.Name())
		if err != nil {
			t.Logf("failed to remove stack. Resources have leaked.")
		}

		return
	}

	res, err := stack.Up(ctx)
	if err != nil {
		t.Logf("up failed, err: %v", err)
	}

	t.Log("up result: ", res)

	assert.NoError(t, err)
}

package test

// This just runs the init function in setup/setup.go

import (
	_ "pulumi-inline-go/setup"

	"os"

	"testing"
)

func TestMain(m *testing.M) {
	code := m.Run()
	os.Exit(code)
}

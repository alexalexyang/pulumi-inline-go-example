package deployHetznerServer

import (
	"fmt"
	"pulumi-inline-go/config"

	"github.com/pulumi/pulumi-hcloud/sdk/go/hcloud"
	"github.com/pulumi/pulumi/sdk/v3/go/common/resource"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"

	"testing"

	"github.com/stretchr/testify/assert"
)

type mocks int

func (mocks) NewResource(args pulumi.MockResourceArgs) (string, resource.PropertyMap, error) {
	return args.Name + "_id", args.Inputs, nil
}

func (mocks) Call(args pulumi.MockCallArgs) (resource.PropertyMap, error) {
	return args.Args, nil
}

func TestInfrastructure(t *testing.T) {
	err := pulumi.RunErr(func(ctx *pulumi.Context) error {

		server, err := hcloud.NewServer(ctx, "serverName", &hcloud.ServerArgs{
			Name:       pulumi.String("serverName"),
			ServerType: pulumi.String(config.HetznerConfig.ServerType),
			Image:      pulumi.String(config.HetznerConfig.OsImage),
			Location:   pulumi.String(config.HetznerConfig.ServerLocation),
			SshKeys:    pulumi.StringArray{pulumi.String("test")},
		})
		if err != nil {
			t.Log("Error with DeployNetworkFunc: ", err)
			return err
		}
		server.Name.ApplyT(func(name string) error {
			t.Log("Name: ", name)
			assert.Equal(t, "serverName", name)
			return nil
		})
		server.ID().ApplyT(func(id pulumi.ID) error {
			t.Log("ID: ", id)

			actual := fmt.Sprintf("%v", id)
			assert.Equal(t, "serverName_id", actual)
			return nil
		})

		server.URN().ApplyT(func(urn string) error {
			t.Log("URN: ", urn)
			assert.Equal(t, "urn:pulumi:stack::project::hcloud:index/server:Server::serverName", urn)
			return nil
		})
		server.ServerType.ApplyT(func(serverType string) error {
			t.Log("ServerType: ", serverType)
			assert.Equal(t, "cx21", serverType)
			return nil
		})

		return nil
	}, pulumi.WithMocks("project", "stack", mocks(0)))
	assert.NoError(t, err)
}

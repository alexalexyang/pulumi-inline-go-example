package rancher2

import (
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/core/v1"
	helmv3 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/helm/v3"

	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v4/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
)

// https://ranchermanager.docs.rancher.com/pages-for-subheaders/install-upgrade-on-a-kubernetes-cluster
func InstallRancher(ctx *pulumi.Context, rancherUrl string) error {

	appLabels := pulumi.StringMap{
		"app": pulumi.String("cattle-system"),
	}

	rancherNamespace, err := corev1.NewNamespace(ctx, "cattle-system", &corev1.NamespaceArgs{
		Metadata: &metav1.ObjectMetaArgs{
			Labels: pulumi.StringMap(appLabels),
			Name:   pulumi.String("cattle-system"),
		},
	})
	if err != nil {
		return err
	}

	_, err = helmv3.NewRelease(ctx, "rancher", &helmv3.ReleaseArgs{
		Chart:     pulumi.String("rancher"),
		Name:      pulumi.String("rancher"),
		Namespace: rancherNamespace.Metadata.Name(),
		RepositoryOpts: helmv3.RepositoryOptsArgs{
			Repo: pulumi.String("https://releases.rancher.com/server-charts/stable"),
		},
		Values: pulumi.Map{
			"hostname": pulumi.String(rancherUrl),
			// "ingress.tls.source": pulumi.String("secret"),
		},
		// Version: pulumi.String(rancherVersion),
	},
	// pulumi.DependsOn([]pulumi.Resource{certmanagerChart, rancherCertificate})
	)

	if err != nil {
		return err
	}
	return nil
}

package setup

import (
	"log"
	"pulumi-inline-go/config"
	getPulumi "pulumi-inline-go/pulumi-installer"
)

func init() {
	log.Println("Setting up environment")

	config.SetSecrets()

	// Must be called before getting Pulumi
	config.SetConfig()

	config.SetHetznerConfig()

	// -- Get Pulumi --
	getPulumi.GetPulumiWorkspaceBackendDir()
	getPulumi.GetPulumi()

	log.Println("Environment set up")
}

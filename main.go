package main

import (
	"context"
	"fmt"
	"log"
	"os"

	// certManager "pulumi-inline-go/cert-manager"
	"pulumi-inline-go/config"
	"pulumi-inline-go/fluxcd"
	hetznerServers "pulumi-inline-go/hetzner/deploy-server"
	getPulumi "pulumi-inline-go/pulumi-installer"
	pulumiUtil "pulumi-inline-go/pulumi-util"
	"pulumi-inline-go/rke2"

	"github.com/joho/godotenv"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"

	"github.com/pulumi/pulumi/sdk/v3/go/auto"
)

func main() {
	destroy := true

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file: ", err)
	}

	config.SetSecrets()
	config.SetConfig()
	config.SetHetznerConfig()
	getPulumi.GetPulumiWorkspaceBackendDir()
	getPulumi.GetPulumi()

	ctx := context.Background()

	stackName := "dev-stack"
	projectName := config.Config.ProjectName
	var opts = pulumiUtil.GetPulumiStackArgs(stackName)

	numServers := 2

	deployFunc := func(ctx *pulumi.Context) error {
		serverInfoSlice, _, err := hetznerServers.DeployServers1SSHKey(ctx, numServers)
		if err != nil {
			log.Fatalln("Error with DeployNetworkFunc: ", err)
			return err
		}

		for idx, serverInfo := range serverInfoSlice {
			ctx.Export(fmt.Sprintf("%s-server-%d-connect-info", config.Config.ProjectName, idx+1), serverInfo.ConnectArgs)
		}

		rke2Server := serverInfoSlice[0]
		rke2Agent := serverInfoSlice[1]

		installServerRes, err := rke2.InstallServer(ctx, &rke2Server.ConnectArgs, []pulumi.Resource{rke2Server.Server})
		if err != nil {
			log.Fatalln("Error installing RKE2 server: ", err)
			return err
		}

		serverChan := make(chan string)
		rke2Server.Server.Ipv4Address.ApplyT(func(ip string) string {
			serverChan <- ip
			return ip
		})

		serverIp := <-serverChan
		close(serverChan)

		kubeconfig, err := rke2.GetKubeconfig(ctx, &rke2Server.ConnectArgs, installServerRes)
		if err != nil {
			log.Fatalln("Error getting kubeconfig: ", err)
			return err
		}
		fmt.Println("kubeconfig: ", *kubeconfig)
		kubeconfigFilePath := rke2.SetKubeconfigPath(*kubeconfig, serverIp)

		serverToken, err := rke2.GetRke2ServerToken(ctx, &rke2Server.ConnectArgs, installServerRes)
		if err != nil {
			log.Fatalln("Error getting RKE2 server token: ", err)
			return err
		}

		fmt.Println("RKE2 server token: ", *serverToken)

		agentChan := make(chan string)
		rke2Agent.Server.Ipv4Address.ApplyT(func(ip string) string {
			fmt.Println("agent ip in ApplyT: ", ip)
			agentChan <- ip
			return ip
		})

		agentIp := <-agentChan
		close(agentChan)

		runScriptRes, err := rke2.InstallAgent(ctx, agentIp, &rke2Agent.ConnectArgs, []pulumi.Resource{rke2Agent.Server})
		if err != nil {
			log.Fatalln("Error installing RKE2 agent: ", err)
			return err
		}

		_, agentStatus, err := rke2.StartAgent(ctx, agentIp, &rke2Agent.ConnectArgs, []pulumi.Resource{runScriptRes}, serverIp, *serverToken)
		if err != nil {
			log.Fatalln("Error starting RKE2 agent: ", err)
		}

		fmt.Println("agentStatus: ", *agentStatus)

		private_key := os.Getenv("GITLAB_SSH_PRIVATE_KEY")
		if private_key == "" {
			log.Fatal("GITLAB_SSH_PRIVATE_KEY env var not set")
		}

		provider, err := fluxcd.CreateNewProvider(ctx, kubeconfigFilePath, private_key)
		if err != nil {
			log.Fatalln("Error creating flux provider: ", err)
			return err
		}

		fmt.Println("provider: ", provider)

		err = fluxcd.BootStrapFlux(ctx, provider, "clusters/staging")
		if err != nil {
			log.Fatalln("Error bootstrapping flux: ", err)
			return err
		}

		return nil
	}

	stack, err := auto.UpsertStackInlineSource(ctx, stackName, projectName, deployFunc, opts...)
	if err != nil {
		log.Fatalln("Error with UpsertStackInlineSource: ", err)
	}

	if destroy {
		stack.Cancel(ctx)
		res, err := stack.Destroy(ctx)
		if err != nil {
			log.Fatalln("destroy failed, err: ", err)
		}

		fmt.Println("destroy result: ", res)

		err = os.Unsetenv("PULUMI_CONFIG_PASSPHRASE")
		if err != nil {
			log.Fatalln("failed to unset EnvVar.")
		}

		err = stack.Workspace().RemoveStack(ctx, stack.Name())
		if err != nil {
			log.Fatalln("failed to remove stack. Resources have leaked.")
		}

		return
	}

	res, err := stack.Up(ctx)
	if err != nil {
		log.Fatalln("up failed, err: ", err)
	}

	fmt.Println("up result: ", res)

}
